import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {
	public static void main(String[] args) throws SQLException {
/*		
		Materia materiaAntiga = new Materia();
		materiaAntiga.setMateria("Portugues");
		materiaAntiga.setAreaConhecimento("Humanas");
		
		Materia materiaNova = new Materia();
		materiaNova.setMateria("Portugues");
		materiaNova.setAreaConhecimento("Exatas");
		
		MateriaDao materiaDao = new MateriaDao();
		materiaDao.atualizarTupla(materiaAntiga, materiaNova);
		
		MateriaDao materiaDao = new MateriaDao();
		Materia materia = materiaDao.consultarTupla("Geografia");
		System.out.println("Materia: " + materia.getMateria() + " | " + "Area de Conhecimento: " + materia.getAreaConhecimento());	
		
		MateriaDao materiaDao = new MateriaDao();
		materiaDao.deletar("Historia");
				
		MateriaDao materiaDao = new MateriaDao();
		List<Materia> materias = materiaDao.consultar();
	    for (Materia materia : materias) {
	    	System.out.println("Materia: " + materia.getMateria() + " | " + "Area de Conhecimento: " + materia.getAreaConhecimento());	
	    }
*/		
		Materia materia = new Materia();
		materia.setMateria("Ingles");
		materia.setAreaConhecimento("Biologica");
		
		MateriaDao materiaDao = new MateriaDao();
		materiaDao.inserir(materia);
		System.out.println("Inclusão efetuada.");
		
		
	}

}
