import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MateriaDao {
	private Connection conexao;
	
//	public MateriaDao() {
//		this.conexao = Conexao.getConnection();
//	}

	public void inserir(Materia materia) {
		this.conexao = Conexao.getConnection();
		String query = "insert into materia (materia, areaConhecimento)" + "values (?,?)";;
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, materia.getMateria());
			statement.setString(2, materia.getAreaConhecimento());
			
			statement.execute();
			statement.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		try {
			this.conexao.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Materia> consultar(){
		String query = "Select * from materia";
		List<Materia> materias = new ArrayList<Materia>();
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			ResultSet resultado = statement.executeQuery();
			
			while (resultado.next()) {
				
				Materia materia = new Materia();
				materia.setMateria(resultado.getString("materia"));
				materia.setAreaConhecimento(resultado.getString("areaConhecimento"));
				materias.add(materia);
			}
			resultado.close();
			statement.close();
			return materias;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void deletar(String materia) {
	//	Materia retMateria = new Materia();
		
		String query = "delete from materia where materia = ?";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, materia);
			statement.execute();
			statement.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Materia consultarTupla(String nomeMateria){
		String query = "Select * from materia where materia = ?";
		Materia materia = new Materia();
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, nomeMateria);
			ResultSet resultado = statement.executeQuery();
			resultado.next();
			materia.setMateria(resultado.getString("materia"));
			materia.setAreaConhecimento(resultado.getString("areaConhecimento"));
			resultado.close();
			statement.close();
			return materia;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void atualizarTupla(Materia materiaAntiga, Materia materiaNova ) {
		String query = "Update materia set areaConhecimento = ? where materia = ?";
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, materiaNova.getAreaConhecimento());
			statement.setString(2, materiaAntiga.getMateria());
			statement.execute();
			statement.close();
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		
	}
}
